# Spring boot - Register Demo

* El proyecto usa:
    * Maven (gestor de dependencias)
    * H2 (base de datos en memoria)
    * JPA (para conseguir mayor nivel de abstracción sobre la base de datos)
    * Lombok (para creacion simple y rapida de DTOs)
    * MapStruct (permite mediante anotaciones crear mapeos entre objetos en tiempo de compilación. Evitando tener que escribir todo el código de mapeo a mano)
    * Swagger (para generar documentación de APIs RESTful y un sandbox para probar las diferentes peticiones)

## Instalación

1. Instalar STS (Spring Tool Suite 4 o superior)
2. Help->Install New Software->Buscar "https://projectlombok.org/p2" e instar Lombok (https://projectlombok.org/setup/eclipse)
3. Help->Marketplace e instalar "MapStruct Eclipse Plugin"
4. Desde STS importar el proyecto. File->Import->Existing Maven Projects->Browse->Select the folder register-demo -> Finish

## Ejecución

1. Arrancar servidor desde STS (Click derecho en el proyecto y Run As->Spring Boot App)
2. Acceder a la dirección "localhost:8090/register/swagger-ui.html" desde el navegador para ver Swagger UI
3. Acceder a la dirección "localhost:8090/register/h2-console" desde el navegador para acceder a la base de datos H2

## Ayuda

* Arquitectura DOMA (Domain Oriented Microservice Architecture)
    * Esta arquitectura es pensada en pro de desacoplar la capa de dominio de la capa de pesistencia, de modo que se pueda usar diferentes bases de datos (SQL o NoSQL)
    * El proyecto se estructuro en las capas principales Web, Dominio y Persistencia
* Patrones:
    * Data Mapper (Adapter) -> Apoya la arquitectura DOMA precisamente para relacionar el mapeo de los DTOs del domino con las entidades
    * Facade -> Ayuda a estructurar el proyecto en capas, en este caso capas como controllers, services, repositories, models
    * Proxy -> Un proxy controla el acceso al objeto original, permitiéndote hacer algo antes o después de que la solicitud llegue al objeto original. A través de la implementacion del interceptor (GlobalExceptionHandler) ya que se maneja de manera cetral las excepciones de todas las peticiones HTTP

## Paquetes

* Main: clase DemoApplication en la carpeta com.register.demo
* Controller (controladores): carpeta com.register.demo.web.controller
* Service (servicios): carpeta com.register.demo.domain.service
* Repository (repositorios): carpetas com.register.demo.domain.repository y com.register.demo.persistence.repository
* Model (modelo): carpeta com.register.demo.persistence.model (Entities)
* Mapper (mapeadores): carpeta com.register.demo.persistence.mapper
* Config (configuracion): carpeta com.register.demo.web.config (Swagger y CORS - Cross Origin Resource Sharing)
* Exception (manejo de excepciones): carpeta com.register.demo.web.exception (Interceptor)
* Itil (utilidades): carpeta com.register.demo.web.util (Contantes)
* Parámetros de configuración: fichero application.properties en la carpeta /src/main/resources

## Adicional

* Para la actualizacion del procesado de registros (1 o varios) se implemento en lote (batch operation) con el fin de aportar un mejor rendimiento.
* 2 operaciones GET para obtener todos los registros y POST para crear un registro
    * /api/v1/records GET
    * /api/v1/records POST
* 1 operacion PUT en lote para la actualizacion de varios registros en una sola peticion
    * /batch/api/v1/records PUT