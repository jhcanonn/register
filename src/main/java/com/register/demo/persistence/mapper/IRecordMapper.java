package com.register.demo.persistence.mapper;

import java.util.List;

import org.mapstruct.Mapper;

import com.register.demo.domain.dto.RecordDomainDTO;
import com.register.demo.persistence.model.Record;

@Mapper(componentModel = "spring")
public interface IRecordMapper {

	RecordDomainDTO toRecordDomain(Record record);

	List<RecordDomainDTO> toRecordsDomain(List<Record> records);

	Record toRecord(RecordDomainDTO recordDomainDTO);

}
