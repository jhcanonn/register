package com.register.demo.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.register.demo.persistence.model.Record;

public interface IRecordPersistenceRepository extends JpaRepository<Record, Integer> {

}
