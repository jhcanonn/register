package com.register.demo.persistence.repository;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.register.demo.domain.dto.RecordDomainDTO;
import com.register.demo.domain.repository.IRecordDomainRepository;
import com.register.demo.persistence.mapper.IRecordMapper;
import com.register.demo.persistence.util.BatchQueries;

@Repository
public class RecordPersistenceRepository implements IRecordDomainRepository {

	@Autowired
	private IRecordPersistenceRepository recordRepository;

	@Autowired
	private IRecordMapper recordMapper;

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	public List<RecordDomainDTO> getAll() {
		return recordMapper.toRecordsDomain(recordRepository.findAll());
	}
	
	@Override
	public RecordDomainDTO save(RecordDomainDTO recordDomainDTO) {
		return recordMapper.toRecordDomain(recordRepository.save(recordMapper.toRecord(recordDomainDTO)));
	}
	
	@Override
	public int[] batchUpdate(List<RecordDomainDTO> recordDomainDTOs) {
		return jdbcTemplate.batchUpdate(BatchQueries.UPDATE_RECODS,
				new BatchPreparedStatementSetter() {
					@Override
					public void setValues(PreparedStatement ps, int i) throws SQLException {
						ps.setBoolean(1, recordDomainDTOs.get(i).getProcessed());
						ps.setInt(2, recordDomainDTOs.get(i).getId());
					}	

					@Override
					public int getBatchSize() {
						return recordDomainDTOs.size();
					}
				});
	}

}
