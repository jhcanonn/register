package com.register.demo.domain.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.register.demo.domain.dto.RecordDomainDTO;
import com.register.demo.domain.dto.RecordProcessedDTO;
import com.register.demo.domain.dto.RecordSaveRequestDTO;
import com.register.demo.domain.repository.IRecordDomainRepository;

@Service
public class RecordService {

	@Autowired
	private IRecordDomainRepository iRecordDomainRepository;

	public List<RecordDomainDTO> getAll() {
		return iRecordDomainRepository.getAll();
	}

	public RecordDomainDTO save(RecordSaveRequestDTO recordSaveRequestDTO) {
		RecordDomainDTO recordDomainDTO = new RecordDomainDTO();
		recordDomainDTO.setFirstName(recordSaveRequestDTO.getFirstName());
		recordDomainDTO.setLastName(recordSaveRequestDTO.getLastName());
		recordDomainDTO.setProcessed(false);
		return iRecordDomainRepository.save(recordDomainDTO);
	}

	public int[] batchUpdate(List<RecordProcessedDTO> listRecordProcessed) {
		List<RecordDomainDTO> listRecordDomain = listRecordProcessed.stream().map(record -> {
			RecordDomainDTO recordDomainDTO = new RecordDomainDTO();
			recordDomainDTO.setId(record.getId());
			recordDomainDTO.setProcessed(record.getProcessed());
			return recordDomainDTO;
		}).collect(Collectors.toList());
		return iRecordDomainRepository.batchUpdate(listRecordDomain);
	}

}
