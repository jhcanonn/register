package com.register.demo.domain.repository;

import java.util.List;

import com.register.demo.domain.dto.RecordDomainDTO;

public interface IRecordDomainRepository {

	public List<RecordDomainDTO> getAll();
	
	public RecordDomainDTO save(RecordDomainDTO recordDomainDTO);
	
	public int[] batchUpdate(List<RecordDomainDTO> recordDomainDTO);

}
