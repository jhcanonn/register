package com.register.demo.domain.dto;

import java.io.Serializable;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

@Data
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class RecordDomainDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	Integer id;
	String firstName;
	String lastName;
	Boolean processed;

}
