package com.register.demo.web.config;

import java.util.ArrayList;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.RestController;

import com.register.demo.web.util.SwaggerConstants;

import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.VendorExtension;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfiguration {
	
	public static final Contact DEFAULT_CONTACT = new Contact(
				SwaggerConstants.CONTACT_NAME,
				SwaggerConstants.CONTACT_URL,
				SwaggerConstants.CONTACT_EMAIL);
	
	@SuppressWarnings("rawtypes")
	public static final ApiInfo DEFAULT_API_INFO = new ApiInfo(
			SwaggerConstants.API_INFO_TITLE,
			SwaggerConstants.API_INFO_DESCRIPTON,
			SwaggerConstants.API_INFO_VERSION,
			SwaggerConstants.API_INFO_TOS_URL,
			DEFAULT_CONTACT,
			SwaggerConstants.API_INFO_LICENSE,
			SwaggerConstants.API_INFO_LICENSE_URL,
			new ArrayList<VendorExtension>());
	
	@Bean
	public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2)
				.apiInfo(DEFAULT_API_INFO)
				.select()
				.apis(RequestHandlerSelectors.withClassAnnotation(RestController.class))
				.paths(PathSelectors.any())
				.build()
				.useDefaultResponseMessages(false);
	}

}
