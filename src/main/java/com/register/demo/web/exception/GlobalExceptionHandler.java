package com.register.demo.web.exception;

import java.util.Arrays;
import java.util.stream.Collectors;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler(Exception.class)
	public ResponseEntity<ApiError> handleDefaultException(Exception ex) {
		return buildResponseEntity(ex, HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	@ExceptionHandler(value = {NullPointerException.class, ArithmeticException.class})
	public ResponseEntity<ApiError> handleNullPointerException(Exception ex) {
		return buildResponseEntity(ex, HttpStatus.NOT_ACCEPTABLE);
	}
	
	private ResponseEntity<ApiError> buildResponseEntity(Exception ex, HttpStatus status) {
		String message = ex.getClass() + ": " + ex.getMessage();
		String debugMessage = Arrays.asList(ex.getStackTrace()).stream().map(n -> String.valueOf(n)).collect(Collectors.joining("|"));
		ApiError apiError = new ApiError(status, message, debugMessage);
		return new ResponseEntity<>(apiError, status);
	}

}
