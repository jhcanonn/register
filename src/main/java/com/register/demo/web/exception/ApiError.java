package com.register.demo.web.exception;

import java.io.Serializable;
import java.time.LocalDateTime;

import org.springframework.http.HttpStatus;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

@Data
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ApiError implements Serializable {

	private static final long serialVersionUID = 1L;

	String status;
	Integer code;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm:ss")
	LocalDateTime timestamp;
	String message;
	String debugMessage;

	public ApiError(HttpStatus status, String message, String debugMessage) {
		super();
		this.timestamp = LocalDateTime.now();
		this.status = status.getReasonPhrase();
		this.code = status.value();
		this.message = message;
		this.debugMessage = debugMessage;
	}

}
