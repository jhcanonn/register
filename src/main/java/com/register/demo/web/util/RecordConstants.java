package com.register.demo.web.util;

public class RecordConstants {
	
	public static final String BATCH_PATH = "/batch";
	public static final String API_V1_PATH = "/api/v1";
	public static final String RECORD_CONTROLLER_PATH = API_V1_PATH + "/records";
	public static final String BATCH_RECORD_CONTROLLER_PATH = BATCH_PATH + API_V1_PATH + "/records";

}
