package com.register.demo.web.util;

public class SwaggerConstants {
	
	public static final String CONTACT_NAME = "Jair Cañon";
	public static final String CONTACT_URL = "https://www.linkedin.com/in/jair-ca%C3%B1on-b84726132/";
	public static final String CONTACT_EMAIL = "jhcanonn@gmail.com";
	
	public static final String API_INFO_TITLE = "Register Api Documentation";
	public static final String API_INFO_DESCRIPTON = "Technical test backend developer with SpringBoot";
	public static final String API_INFO_VERSION = "1.0";
	public static final String API_INFO_TOS_URL = "PREMIUM";
	public static final String API_INFO_LICENSE = "Apache 2.0";
	public static final String API_INFO_LICENSE_URL = "http://www.apache.org/licenses/LICENSE-2.0";

}
