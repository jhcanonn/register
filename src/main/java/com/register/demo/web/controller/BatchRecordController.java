package com.register.demo.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.register.demo.domain.dto.RecordProcessedDTO;
import com.register.demo.domain.service.RecordService;
import com.register.demo.web.util.RecordConstants;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(RecordConstants.BATCH_RECORD_CONTROLLER_PATH)
public class BatchRecordController {
	
	@Autowired
	private RecordService recordService;
	
	@PutMapping
	@ResponseStatus(code = HttpStatus.MULTI_STATUS)
	@ApiOperation("Update batch of records")
	public int[] batchUpdate(@RequestBody List<RecordProcessedDTO> listRecordProcessed) {
		return recordService.batchUpdate(listRecordProcessed);
	}
	
}
