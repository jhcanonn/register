package com.register.demo.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.register.demo.domain.dto.RecordDomainDTO;
import com.register.demo.domain.dto.RecordSaveRequestDTO;
import com.register.demo.domain.service.RecordService;
import com.register.demo.web.util.RecordConstants;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(RecordConstants.RECORD_CONTROLLER_PATH)
public class RecordController {

	@Autowired
	private RecordService recordService;

	@GetMapping
	@ApiOperation("Get all records")
	public List<RecordDomainDTO> getAll() {
		return recordService.getAll();
	}

	@PostMapping
	@ResponseStatus(code = HttpStatus.CREATED)
	@ApiOperation("Save a record")
	public RecordDomainDTO save(@RequestBody RecordSaveRequestDTO recordSaveRequestDTO) {
		return recordService.save(recordSaveRequestDTO);
	}

}
